/*
 * terNixI2CDriver.c
 *
 *  Created on: Sep 1, 2015
 *      Author: teng
 */

#include "terNixI2cDriver.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stropts.h>
#include <math.h>
#include "i2c-dev.h"

int  _i2cBus;
int  _i2cDevice;
int  _i2cBusFile;

int i2c_write_data(const char* buff, int len);
int i2c_read_data(char* buff, int len);

#define MAX_BUS 64

void initI2cDriver(int i2cBus)
{
	_i2cBus            = i2cBus;
	_i2cBusFile        = 0;
	_i2cDevice         = 0;

    open_i2c_bus(_i2cBus);
}

int open_i2c_bus(int bus)
{
    int file;
    char namebuf[MAX_BUS];

    snprintf(namebuf, sizeof(namebuf), "/dev/i2c-%d", bus);

    if ((file = open(namebuf, O_RDWR)) < 0) {
        fprintf(stderr, "I2CDRIVER: Failed to open I2C Bus: %d\n", bus);
        return -1;
    }

    _i2cBus = bus;
    _i2cBusFile = file;

    return 1;
}

int close_i2c_bus()
{
    if(_i2cBusFile == 0) {
        fprintf(stderr, "I2CDRIVER: Failed to close I2C bus, no I2C bus is opened.\n");
        return -1;
    }

    if(close(_i2cBusFile) < 0) {
        fprintf(stderr, "I2CDRIVER: Failed to close I2C-%d\n", _i2cBus);
        return -1;
    }

    _i2cBus = 0;
    _i2cDevice = 0;
    _i2cBusFile = 0;

    return 1;
}

int open_i2c_device(int address)
{
    if(_i2cBusFile == 0) {
    	fprintf(stderr, "I2CDRIVER: Failed to open I2C device: %d as no I2C bus opened. \n", address);
        return -1;
    }

    if(_i2cDevice != address) {
        if(ioctl(_i2cBusFile, I2C_SLAVE, address) < 0) {
        	fprintf(stderr, "I2CDRIVER: Failed to open I2C device: %d\n", address);
            return -1;
        }
        _i2cDevice = address;
    }
    return 1;
}

int i2c_msg_transceiver(I2CMsgBuff *msg)
{
    int result = 1;

    if(open_i2c_device(msg->i2cAddress) < 0)
        return -1;

    if(msg->cmd == 0) {
        if(msg->rBuf.Len == 0)
            result = i2c_smbus_write_quick(_i2cBusFile, I2C_SMBUS_WRITE);
        else if(msg->rBuf.Len == 1) {
            result = i2c_smbus_read_byte(_i2cBusFile);

            if(result >= 0)
                msg->rBuf.Data[0] = (u_int8_t)result;
        }
        else
            result = -1;

        return result;
    }

    switch(msg->tBuf.Len) {
        case 0:
            switch(msg->rBuf.Len) {
                case 0: // write byte
                    result = i2c_smbus_write_byte(_i2cBusFile, msg->cmd);
                    break;
                case 1: // read byte
                    result = i2c_smbus_read_byte_data(_i2cBusFile, msg->cmd);
                    if(result >= 0)
                        msg->rBuf.Data[0] = (u_int8_t)result;
                    break;
                case 2: // read word
                    result = i2c_smbus_read_word_data(_i2cBusFile, msg->cmd);
                    if(result >= 0) {
                        msg->rBuf.Data[0] = (u_int8_t)(result & 0xff);
                        msg->rBuf.Data[1] = (u_int8_t)(result >> 8);
                    }
                    break;
                default: // read block
                    result = i2c_smbus_read_i2c_block_data(_i2cBusFile, msg->cmd, msg->rBuf.Len, msg->rBuf.Data);
                    if(result != msg->rBuf.Len)
                        result = -1;
                    break;
            }
            break;
        case 1:
            if(msg->rBuf.Len != 0)
                result = i2c_smbus_block_process_call(_i2cBusFile, msg->cmd, msg->tBuf.Len, msg->tBuf.Data, msg->rBuf.Len, msg->rBuf.Data);
            else
                result = i2c_smbus_write_byte_data(_i2cBusFile, msg->cmd, msg->tBuf.Data[0]);
            break;
        case 2:
            result = i2c_smbus_write_word_data(_i2cBusFile, msg->cmd, (msg->tBuf.Data[0]|(msg->tBuf.Data[1] << 8)));
            break;
        default:
            result = i2c_smbus_write_i2c_block_data(_i2cBusFile, msg->cmd, msg->tBuf.Len, msg->tBuf.Data);
            break;
    }

    return result;
}

int i2c_write_data(const char *buff, int len)
{
    if(_i2cDevice == 0) {
        fprintf(stderr, "I2CDRIVER: Failed to write i2c data as no I2C device is opened.\n");
        return -1;
    }

    if(_i2cBusFile == 0) {
    	fprintf(stderr, "I2CDRIVER: Failed to write i2c data as no I2C bus is opened.\n");
        return -1;
    }

    if(write(_i2cBusFile, buff, len) != len) {
    	fprintf(stderr, "I2CDRIVER: Failed to write data to slave: %d through I2C-%d\n", _i2cDevice, _i2cBus);
        return -1;
    }

    return 1;
}

int i2c_read_data(char *buff, int len)
{
    if(_i2cDevice == 0) {
        fprintf(stderr, "I2CDRIVER: Failed to read i2c data as no I2C device is opened.\n");
        return -1;
    }

    if(_i2cBusFile == 0) {
        fprintf(stderr, "I2CDRIVER: Failed to read i2c data as no I2C bus is opened.\n");
        return -1;
    }

    if (read(_i2cBusFile, buff, len) != len){
        fprintf(stderr, "I2CDRIVER: Failed to read data from slave: %d through I2C-%d\n", _i2cDevice, _i2cBus);
        return -1;
    }

    return 1;
}
