/*
 * terNixI2CDriver.h
 *
 *  Created on: Sep 1, 2015
 *      Author: teng
 */

#ifndef TERNIXI2CDRIVER_H_
#define TERNIXI2CDRIVER_H_

#include "glib.h"


#define TERNIX_I2C_BUS_NUM	2

#define I2C_BUS_1   1
#define I2C_BUS_2   2

typedef struct {
    guchar cmd;
    guchar i2cAddress;

    struct {
        guchar Len;
        guchar* Data;
    } tBuf;

    struct {
        guchar Len;
        guchar* Data;
    } rBuf;
} I2CMsgBuff;

void initI2cDriver(int i2cBus);

int open_i2c_bus(int bus);
int close_i2c_bus(void);
int open_i2c_device(int address);

int i2c_msg_transceiver(I2CMsgBuff* msg);

#endif /* TERNIXI2CDRIVER_H_ */
