/*
 * terNixI2cTask.c
 *
 *  Created on: Sep 2, 2015
 *      Author: teng
 */

#include "terNixI2cTask.h"
#include "stdlib.h"
#include "stdio.h"

typedef struct {
	I2CMsgBuff msg;
	I2CTaskCallBack callback;
	gpointer parameter;
} I2cTaskItem;

typedef struct i2cFIFONode {
	I2cTaskItem item;
	struct i2cFIFONode * next;
} I2cFIFONode;

GMutex _terNixI2cFIFOMutex;
GMutex _terNixI2cTaskControlMutex;
GCond  _terNixI2cTaskWaitCond;

I2cFIFONode * _terNixI2cFIFO;

gboolean _ternixI2cTaskRunningFlag = FALSE;

gboolean isTerNixI2cFIFOEmpty(void);
void clearAllTerNixI2cFIFO(void);
void removeOneTerNixI2cFIFOItemFromHead(void);
void addTerNixI2cFIFOItemToEnd(I2cFIFONode * node);
gboolean isTerNixI2cFIFOFull(void);
I2cTaskItem getNextI2cFIFOItem(void);
gpointer i2cTaskRun (gpointer data);

static gboolean isI2CTaskRunning(void);
static void i2cDebugLog(guchar cmd);

void initI2cTask(int i2cBus)
{
    initI2cDriver(i2cBus);
}

ErrorCode runSingleI2cSessionTask(I2CMsgBuff *buff)
{
    ErrorCode result = Success;

    if((i2c_msg_transceiver(buff)) < 0)
        result = DataTransferFail;

    return result;
}

I2cFIFONode * createNewI2cFIFONode(guchar i2cAddress, guchar cmd, guchar tLen, guchar rLen, guchar *tBuff, guchar *rBuff, I2CTaskCallBack callback, gpointer para)
{
	I2cFIFONode * newNode = NULL;

	newNode = (I2cFIFONode *)malloc(sizeof(I2cFIFONode));

	if(newNode == NULL)
		return NULL;

	newNode->item.msg.i2cAddress = i2cAddress;
	newNode->item.msg.cmd = cmd;
	newNode->item.msg.rBuf.Len = rLen;
	newNode->item.msg.rBuf.Data = rBuff;
	newNode->item.msg.tBuf.Len = tLen;
	newNode->item.msg.tBuf.Data = tBuff;
	newNode->item.callback = callback;
	newNode->item.parameter = para;
	newNode->next = NULL;

	return newNode;
}

ErrorCode addNewI2cSession(I2CTaskCallBack callback, gpointer para, guchar i2cAddress, guchar cmd, guchar tLen, guchar rLen, guchar *tBuff, guchar *rBuff)
{
	static gint64 count = 0;

    ErrorCode result = Success;
    I2cFIFONode * newNode;

    if(isTerNixI2cFIFOFull())
    	return TaskFIFOisFull;

    newNode = createNewI2cFIFONode(i2cAddress, cmd, tLen, rLen, tBuff, rBuff, callback, para);

    addTerNixI2cFIFOItemToEnd(newNode);

    printf("[%llu]Add CMD Node: ", count++);
	printf("Param: %p, Address: %u, tBuff: %p, rBuff: %p ", newNode->item.parameter, newNode->item.msg.i2cAddress, newNode->item.msg.tBuf.Data, newNode->item.msg.rBuf.Data);
    i2cDebugLog(cmd);
    fflush(stdout);

    return result;
}

ErrorCode isI2cDeviceReadyForSession(guchar i2cAddress, I2CTaskCallBack callback)
{
    ErrorCode result = Success;

    result = addNewI2cSession(callback, 0, i2cAddress, CMD_NULL, I2C_TERNIX_NULL_MESSAGE_LEN, I2C_TERNIX_NULL_MESSAGE_LEN, NULL, NULL);

    return result;
}

gboolean isTerNixI2cFIFOEmpty(void)
{
	gboolean isEmpty = FALSE;

	g_mutex_lock(&_terNixI2cFIFOMutex);

	if(_terNixI2cFIFO == NULL) isEmpty = TRUE;

	g_mutex_unlock(&_terNixI2cFIFOMutex);

	return isEmpty;
}

void clearAllTerNixI2cFIFO(void)
{
	while(!isTerNixI2cFIFOEmpty())
		removeOneTerNixI2cFIFOItemFromHead();
}

gboolean isTerNixI2cFIFOFull(void)
{
	gboolean isFull;
	I2cFIFONode * node;

	node = (I2cFIFONode *)malloc(sizeof(I2cFIFONode));

	if(node == NULL)
		isFull = TRUE;
	else
		isFull = FALSE;

	free(node);

	return isFull;
}

I2cTaskItem getNextI2cFIFOItem(void)
{
	I2cTaskItem item;

	g_mutex_lock(&_terNixI2cFIFOMutex);
	item = _terNixI2cFIFO->item;
	g_mutex_unlock(&_terNixI2cFIFOMutex);

	return item;
}

void removeOneTerNixI2cFIFOItemFromHead(void)
{
	I2cFIFONode * node;

	g_mutex_lock(&_terNixI2cFIFOMutex);

	if(_terNixI2cFIFO->next == NULL) {
		free(_terNixI2cFIFO);
		_terNixI2cFIFO = NULL;
	} else {
		node = _terNixI2cFIFO->next;
		free(_terNixI2cFIFO);
		_terNixI2cFIFO = node;
	}

	g_mutex_unlock(&_terNixI2cFIFOMutex);
}

void addTerNixI2cFIFOItemToEnd(I2cFIFONode * node)
{
	g_mutex_lock(&_terNixI2cFIFOMutex);

	I2cFIFONode * preNode = _terNixI2cFIFO;

	if(preNode == NULL)
		_terNixI2cFIFO = node;
	else {
		while(preNode->next != NULL) {
			preNode = preNode->next;
		}
		preNode->next = node;
	}

	g_cond_broadcast(&_terNixI2cTaskWaitCond);

	g_mutex_unlock(&_terNixI2cFIFOMutex);
}

void startI2cTask(void)
{
	if(!isI2CTaskRunning()) {
		g_mutex_lock(&_terNixI2cTaskControlMutex);
		_ternixI2cTaskRunningFlag = TRUE;
		g_mutex_unlock(&_terNixI2cTaskControlMutex);
	    g_thread_new("terNix I2C Task", i2cTaskRun, NULL);
	}
}

void stopI2cTask(void)
{
	g_mutex_lock(&_terNixI2cTaskControlMutex);
	_ternixI2cTaskRunningFlag = FALSE;
	clearAllTerNixI2cFIFO();
	g_mutex_unlock(&_terNixI2cTaskControlMutex);
	close_i2c_bus();
}

static gboolean isI2CTaskRunning(void)
{
	gboolean running = FALSE;

	g_mutex_lock(&_terNixI2cTaskControlMutex);
	running = _ternixI2cTaskRunningFlag;
	g_mutex_unlock(&_terNixI2cTaskControlMutex);

	return running;
}

gpointer i2cTaskRun(gpointer data)
{
	gint64 count = 0;
	I2cTaskItem item;
	guchar try;
	ErrorCode rslt;

	while(isI2CTaskRunning()) {
		g_mutex_lock(&_terNixI2cFIFOMutex);
		while(_terNixI2cFIFO == NULL)
			g_cond_wait(&_terNixI2cTaskWaitCond, &_terNixI2cFIFOMutex);
		g_mutex_unlock(&_terNixI2cFIFOMutex);

		item = getNextI2cFIFOItem();

		for(try = 0; try < MAX_TRY_FOR_SINGLE_SESSION; try++) {
			rslt = runSingleI2cSessionTask(&(item.msg));
			if(rslt == Success) {
				printf("[%llu]CMD send succeed: ", count);
				printf("Param: %p, Address: %u, tBuff: %p, rBuff: %p ", item.parameter, item.msg.i2cAddress, item.msg.tBuf.Data, item.msg.rBuf.Data);
				i2cDebugLog(item.msg.cmd);
				fflush(stdout);
				break;
			}
		}
		count++;
		item.callback(item.msg.i2cAddress, item.msg.cmd, item.parameter, rslt);
		removeOneTerNixI2cFIFOItemFromHead();

		g_thread_yield();
	}
	g_thread_exit(data);

	return NULL;
}

static void i2cDebugLog(guchar cmd)
{
	switch(cmd) {
	case CMD_NULL:
		printf("I2C device connect query\n"); break;
	case CMD_BOOTLOADER_RESET:
		printf("Bootloader reset\n"); break;
	case CMD_BOOTLOADER_FLASH_MSG_WRITE:
		printf("Bootloader flash msg write\n"); break;
	case CMD_BOOTLOADER_DATA_WAIT:
		printf("Bootloader data write\n"); break;
	case CMD_BOOTLOADER_CRC_READ:
		printf("Bootloader CRC read\n"); break;
	case CMD_BOOTLOADER_VERSION_READ:
		printf("Bootloader version read\n"); break;
	case CMD_BOOTLOADER_QUERY_STATE:
		printf("Bootloader query state\n"); break;
	case CMD_ANALOG_SENSOR_DATA_READ:
		printf("analog sensor read\n"); break;
	case CMD_BOOTLOADER_CRC_CALCULATE:
		printf("Bootloader CRC calculate\n"); break;
	case CMD_PROPO_CHANNEL_SET_PWM_DC:
		printf("prop channel set PWM DC\n"); break;
	case CMD_PROPO_CHANNEL_SET_PWM_FREQ:
		printf("propo channel set PWM frequency\n"); break;
	case CMD_PROPO_MODULE_INIT_CONFIG:
		printf("propo module init configuration\n"); break;
	case CMD_PROPO_CHANNEL_CONFIG:
		printf("propo channel config\n"); break;
	case CMD_ANALOG_MODULE_INIT_CONFIG:
		printf("ananlog module init configuration\n"); break;
	case CMD_ANALOG_CHANNEL_CONFIG:
		printf("analog channel configuration\n"); break;
	case CMD_ANALOG_MODULE_UPATE_STATE_QUERY:
		printf("analog module update state query\n"); break;
	case CMD_ANALOG_MODULE_READ_UPDATED_CHANNELS:
		printf("analog module read update channels\n"); break;
	default: break;
	}
}
