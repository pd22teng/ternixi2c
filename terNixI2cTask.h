/*
 * terNixI2cTask.h
 *
 *  Created on: Sep 2, 2015
 *      Author: teng
 */

#ifndef TERNIXI2CTASK_H_
#define TERNIXI2CTASK_H_

#include "terNixI2cMessage.h"
#include "terNixI2cDriver.h"

#define MAX_TRY_FOR_SINGLE_SESSION 3

typedef enum {
	Success = 0,
	I2CBusOpenFail,
	I2CDeviceOpenFail,
	DataTransferFail,
	TaskStartFail,
	AddSessionFail,
	TaskFIFOisFull,
	CreateNodeFail,
	Other = 0xFF
} ErrorCode;

typedef void (*I2CTaskCallBack)(guchar, guchar, gpointer, gint); // i2cAddress, i2cCMD, parameter, ErrorCode

void initI2cTask(int i2cBus);

ErrorCode addNewI2cSession(I2CTaskCallBack callback, gpointer para, guchar i2cAddress, guchar cmd, guchar tLen, guchar rLen, guchar* tBuff, guchar* rBuff);

ErrorCode isI2cDeviceReadyForSession(guchar i2cAddress, I2CTaskCallBack callback);

void startI2cTask(void);
void stopI2cTask(void);

#endif /* TERNIXI2CTASK_H_ */
